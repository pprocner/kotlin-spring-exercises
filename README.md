# Task

One of the teams used task board to organize their work in project aimed to launch a simple video streaming service. 
The task board was divided into columns: 
 * To Do, 
 * In Progress, 
 * To Verify, 
 * Done.
  
Subsequent system features were placed on cards on the task board and then moved between that columns, as work progressed.
Application works on events: task can be created, moved or deleted (see TaskEvent.kt). 
These events are applied to task board which represents state of the system (see TaskBoard.kt).

**The goal is to make all test passing**. Start with unit tests and once all is green move on to integration tests. 
Follow *TODOs* in the code. Good luck! 

Run all tests:

```./gradlew test integrationTest```

Things to do:
1. Create post endpoint (TaskEndpoint.kt)
2. Create get endpoint (TaskEndpoint.kt)
3. Make 'toDate' parameter optional (TaskEndpoint.kt and TaskBoardService.kt)
4. Add validation (TaskEvent.kt)
5. Use configuration properties (TaskBoardConfiguration.kt)
6. Get rid of null (SampleEvents.groovy) - bonus
