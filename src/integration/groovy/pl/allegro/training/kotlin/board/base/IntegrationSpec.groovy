package pl.allegro.training.kotlin.board.base

import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.ConfigurableMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import pl.allegro.training.kotlin.board.BoardApplication
import spock.lang.Shared
import spock.lang.Specification

import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS

@ContextConfiguration
@SpringBootTest(
        classes = [BoardApplication],
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@TestExecutionListeners(mergeMode = MERGE_WITH_DEFAULTS)
@CompileStatic
abstract class IntegrationSpec extends Specification {

    @Autowired WebApplicationContext webApplicationContext
    @Shared MockMvc mockMvc
    @Shared MockMvcHttpClient mockMvcHttpClient

    void setup() {
        ConfigurableMockMvcBuilder mockMvcBuilder = MockMvcBuilders.webAppContextSetup(webApplicationContext)
        mockMvc = mockMvcBuilder.build()
        mockMvcHttpClient = new MockMvcHttpClient(mockMvc)
    }
}
