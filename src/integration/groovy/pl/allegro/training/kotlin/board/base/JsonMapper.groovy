package pl.allegro.training.kotlin.board.base

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.type.CollectionType
import groovy.json.JsonOutput
import groovy.transform.CompileStatic

import static pl.allegro.training.kotlin.board.task.TaskBoardConfiguration.OBJECT_MAPPER

@CompileStatic
class JsonMapper {
    static ObjectMapper objectMapper = OBJECT_MAPPER

    static String toJson(Object obj) {
        String json = objectMapper.writer()
                .writeValueAsString(obj)
        return JsonOutput.prettyPrint(json)
    }

    static <T> T parseJson(String json, Class<T> expectedType) {
        return objectMapper.readValue(json, expectedType)
    }

    static <T> List<T> parseJsonList(String json, Class<T> expectedType) {
        CollectionType javaType = objectMapper.getTypeFactory()
                .constructCollectionType(List.class, expectedType)
        return objectMapper.readValue(json, javaType)
    }
}
