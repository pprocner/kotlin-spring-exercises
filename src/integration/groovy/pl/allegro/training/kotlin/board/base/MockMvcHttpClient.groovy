package pl.allegro.training.kotlin.board.base

import groovy.transform.CompileStatic
import groovy.transform.PackageScope
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

@CompileStatic
class MockMvcHttpClient {

    private final MockMvc mockMvc

    @PackageScope
    MockMvcHttpClient(MockMvc mockMvc) {
        this.mockMvc = Objects.requireNonNull(mockMvc)
    }

    ResultActions sendGetRequest(String path, Map<String, String> queryParams = [:]) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>()
        queryParams.entrySet().each { params.add(it.key, it.value as String) }
        return mockMvc.perform(
                MockMvcRequestBuilders.get(path)
                        .params(params)
                        .accept(MediaType.APPLICATION_JSON)
        )
    }

    ResultActions postJson(String path, Object dto) {
        return mockMvc.perform(
                MockMvcRequestBuilders.post(path)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonMapper.toJson(dto))
                        .accept(MediaType.APPLICATION_JSON)
        )
    }
}
