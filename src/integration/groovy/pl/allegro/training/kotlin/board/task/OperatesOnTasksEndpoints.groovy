package pl.allegro.training.kotlin.board.task

import groovy.transform.CompileStatic
import groovy.transform.SelfType
import org.springframework.test.web.servlet.ResultActions
import pl.allegro.training.kotlin.board.base.IntegrationSpec

@SelfType(IntegrationSpec)
@CompileStatic
trait OperatesOnTasksEndpoints {

    ResultActions addTaskEvent(TaskEvent taskEvent) {
        return mockMvcHttpClient.postJson("/tasks", taskEvent)
    }

    ResultActions fetchTaskBoard(String date) {
        return mockMvcHttpClient.sendGetRequest("/tasks", [ toDate: date ])
    }
}
