package pl.allegro.training.kotlin.board.task

import org.springframework.test.web.servlet.ResultActions
import pl.allegro.training.kotlin.board.base.IntegrationSpec

import static org.hamcrest.Matchers.equalTo
import static org.hamcrest.collection.IsCollectionWithSize.hasSize
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static pl.allegro.training.kotlin.board.task.SampleEvents.events
import static pl.allegro.training.kotlin.board.task.SampleEvents.taskCreatedEvent

class TaskBoardAcceptanceSpec extends IntegrationSpec implements OperatesOnTasksEndpoints {

    def "should return task board as of 2017-01-15"() {
        given: "there are list of task events"
            List<TaskEvent> events = events()
        and: "task events are added to task board"
            events.collect { addTaskEvent(it) }

        when: "task board as of 2017-01-15 is requested"
            ResultActions resultActions = fetchTaskBoard("2017-01-15")
        then: "task board with IN_PROGRESS and TO_VERIFY tasks is returned"
            resultActions.andExpect(status().isOk())
                    .andExpect(jsonPath("tasks.TO_DO", hasSize(0)))
                    .andExpect(jsonPath("tasks.IN_PROGRESS", hasSize(1)))
                    .andExpect(jsonPath("tasks.TO_VERIFY", hasSize(2)))
                    .andExpect(jsonPath("tasks.DONE", hasSize(0)))
    }

    def "should return final task board"() {
        given: "there are list of task events"
            List<TaskEvent> events = events()
        and: "task events are added to task board"
            events.collect { addTaskEvent(it) }

        when: "final task board is requested"
            ResultActions resultActions = fetchTaskBoard()
        then: "task board without any task is returned"
            resultActions.andExpect(status().isOk())
                    .andExpect(jsonPath("tasks.TO_DO", hasSize(0)))
                    .andExpect(jsonPath("tasks.IN_PROGRESS", hasSize(0)))
                    .andExpect(jsonPath("tasks.TO_VERIFY", hasSize(0)))
                    .andExpect(jsonPath("tasks.DONE", hasSize(0)))
    }

    def "should not allow to add invalid event"() {
        given: "there is task event with negative difficulty"
            TaskEvent invalidEvent = taskCreatedEvent("2017-01-01", "event with invalid difficulty", -1)

        when: "invalid task event is added"
            ResultActions resultActions = addTaskEvent(invalidEvent)
        then: "system does not allow to add invalid task"
            resultActions.andExpect(status().isBadRequest())
    }

    def "should create event with default difficulty"() {
        given: "there is task event with negative difficulty"
            TaskEvent invalidEvent = taskCreatedEvent("2017-01-01", "event with default difficulty")

        when: "task event with default difficulty is added"
            ResultActions resultActions = addTaskEvent(invalidEvent)
        then: "system returns task with default difficulty"
            resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("tasks.TO_DO[0].difficulty", equalTo(3)))
    }
}
