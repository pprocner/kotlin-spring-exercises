package pl.allegro.training.kotlin.board.task

import pl.allegro.training.kotlin.board.task.Column.*

data class TaskBoard(val defaultDifficulty: Int, val tasks: Map<String, Task> = emptyMap()) {

    fun apply(event: TaskEvent): TaskBoard = when (event) {
        is TaskCreatedEvent -> addTask(
                Task(event.taskName, event.difficulty ?: defaultDifficulty, TO_DO)
        )
        is TaskMovedEvent   -> moveTask(event.taskName, event.column)
        is TaskDeletedEvent -> deleteTask(event.taskName)
        else                -> throw IllegalArgumentException("Unknown event type: ${event::class.qualifiedName}")
    }

    private fun addTask(task: Task): TaskBoard = this.copy(tasks = tasks + (task.name to task))

    private fun moveTask(taskName: String, targetColumn: Column): TaskBoard =
            this.copy(tasks = tasks.mapValues { (key, value) ->
                if (key == taskName) value.copy(column = targetColumn) else value
            })

    private fun deleteTask(taskName: String): TaskBoard = this.copy(tasks = tasks - taskName)

    fun dto(): TaskBoardDto {
        return TaskBoardDto(mapOf(
                TO_DO to tasks.values.filter { it.column == TO_DO },
                IN_PROGRESS to tasks.values.filter { it.column == IN_PROGRESS },
                TO_VERIFY to tasks.values.filter { it.column == TO_VERIFY },
                DONE to tasks.values.filter { it.column == DONE }
        ))
    }

    companion object {
        const val DEFAULT_DIFFICULTY = 2
    }
}

data class Task(val name: String, val difficulty: Int, val column: Column)

data class TaskBoardDto(val tasks: Map<Column, List<Task>>)
