package pl.allegro.training.kotlin.board.task

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.afterburner.AfterburnerModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class TaskBoardConfiguration {

    @Bean
    //TODO: Step 5. use value from application.yaml configuration file (use @ConfigurationProperties not @Value - modify TaskBoardProperties class)
    fun taskBoardService() = TaskBoardService(TaskBoard.DEFAULT_DIFFICULTY)

    @Bean
    fun objectMapper(): ObjectMapper = OBJECT_MAPPER

    companion object {
        @JvmStatic
        val OBJECT_MAPPER = ObjectMapper().apply {
            registerModule(JavaTimeModule())
            registerModule(Jdk8Module())
            registerModule(KotlinModule())
            registerModule(AfterburnerModule())
            enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING)
            disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES)
            disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        }
    }
}
