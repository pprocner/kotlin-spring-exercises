package pl.allegro.training.kotlin.board.task

import java.time.LocalDate

class TaskBoardService(private val defaultTaskDifficulty: Int) {
    private var events: MutableList<TaskEvent> = mutableListOf()

    fun addEvent(taskEvent: TaskEvent): TaskBoardDto {
        events.add(taskEvent)
        return taskBoard()
    }

    fun taskBoard(toDate: LocalDate? = null): TaskBoardDto {
        return events
                .takeWhile { event -> beforeDateOrTrue(toDate, event) }
                .fold(TaskBoard(defaultTaskDifficulty)) { board, event -> board.apply(event) }
                .dto()
    }

    private fun beforeDateOrTrue(toDate: LocalDate?, event: TaskEvent) =
            toDate?.let { event.date <= it } ?: true
}
