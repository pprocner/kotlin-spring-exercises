package pl.allegro.training.kotlin.board.task

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.time.LocalDate

enum class Column {
    TO_DO, IN_PROGRESS, TO_VERIFY, DONE
}

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
        JsonSubTypes.Type(value = TaskCreatedEvent::class, name = "TaskCreatedEvent"),
        JsonSubTypes.Type(value = TaskMovedEvent::class, name = "TaskMovedEvent"),
        JsonSubTypes.Type(value = TaskDeletedEvent::class, name = "TaskDeletedEvent")
)
abstract class TaskEvent {
    abstract val date: LocalDate
    abstract val taskName: String
}

//TODO: Step 4. add validation - difficulty should be positive (use javax.validation.constraints.Positive)
data class TaskCreatedEvent(
        override val date: LocalDate,
        override val taskName: String,
        val difficulty: Int? = null
) : TaskEvent()

data class TaskMovedEvent(
        override val date: LocalDate,
        override val taskName: String,
        val column: Column
) : TaskEvent()

data class TaskDeletedEvent(
        override val date: LocalDate,
        override val taskName: String
) : TaskEvent()
