package pl.allegro.training.kotlin.board.task.infrastructure.mvc

/*
TODO: Step 1. Create /tasks post endpoint which would add new task event (validated request body). Use TaskBoardService.
      Endpoint should produce 'application/json' response.

TODO: Step 2. Create /tasks get endpoint with 'toDate' as request param (annotate param with '@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)')
      Use TaskBoardService class.
      Endpoint should produce 'application/json' response.

TODO: Step 3. make 'toDate' parameter optional
 */
class TaskEndpoint
