package pl.allegro.training.kotlin.board.task

import static java.time.LocalDate.parse
import static pl.allegro.training.kotlin.board.task.Column.DONE
import static pl.allegro.training.kotlin.board.task.Column.IN_PROGRESS
import static pl.allegro.training.kotlin.board.task.Column.TO_VERIFY

class SampleEvents {

    static List<TaskEvent> events() {
        return [
                taskCreatedEvent("2017-01-01", "Add account management", 2),
                taskCreatedEvent("2017-01-02", "Implement video upload", 5),
                taskMovedEvent("2017-01-03", "Add account management", IN_PROGRESS),
                taskMovedEvent("2017-01-03", "Implement video upload", IN_PROGRESS),
                taskCreatedEvent("2017-01-04", "Create video search"),
                taskMovedEvent("2017-01-05", "Add account management", TO_VERIFY),
                taskMovedEvent("2017-01-06", "Add account management", DONE),
                taskMovedEvent("2017-01-06", "Implement video upload", TO_VERIFY),
                taskCreatedEvent("2017-01-06", "Add playlist management"),
                taskDeletedEvent("2017-01-07", "Add account management"),
                taskMovedEvent("2017-01-08", "Implement video upload", IN_PROGRESS),
                taskMovedEvent("2017-01-09", "Create video search", IN_PROGRESS),
                taskMovedEvent("2017-01-09", "Implement video upload", TO_VERIFY),
                taskMovedEvent("2017-01-10", "Implement video upload", DONE),
                taskMovedEvent("2017-01-10", "Add playlist management", IN_PROGRESS),
                taskDeletedEvent("2017-01-11", "Implement video upload"),
                taskMovedEvent("2017-01-12", "Add playlist management", TO_VERIFY),
                taskCreatedEvent("2017-01-12", "Implement video comments", 4),
                taskMovedEvent("2017-01-13", "Implement video comments", IN_PROGRESS),
                taskMovedEvent("2017-01-15", "Implement video comments", TO_VERIFY),
                taskMovedEvent("2017-01-16", "Implement video comments", DONE),
                taskMovedEvent("2017-01-16", "Create video search", TO_VERIFY),
                taskDeletedEvent("2017-01-16", "Implement video comments"),
                taskMovedEvent("2017-01-17", "Add playlist management", DONE),
                taskMovedEvent("2017-01-17", "Create video search", DONE),
                taskDeletedEvent("2017-01-18", "Add playlist management"),
                taskDeletedEvent("2017-01-18", "Create video search")
        ]
    }

    private static TaskDeletedEvent taskDeletedEvent(String date, String name) {
        new TaskDeletedEvent(parse(date), name)
    }

    private static TaskMovedEvent taskMovedEvent(String date, String name, Column column) {
        new TaskMovedEvent(parse(date), name, column)
    }

    static TaskCreatedEvent taskCreatedEvent(String date, String name, Integer difficulty) {
        return new TaskCreatedEvent(parse(date), name, difficulty)
    }

    //TODO: Step 6. (bonus :) ) get rid of null - make it possible to call new TaskCreatedEvent(date, name) from groovy
    static TaskCreatedEvent taskCreatedEvent(String date, String name) {
        return new TaskCreatedEvent(parse(date), name, null)
    }
}
