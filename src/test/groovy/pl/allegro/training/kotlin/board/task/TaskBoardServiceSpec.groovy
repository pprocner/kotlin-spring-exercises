package pl.allegro.training.kotlin.board.task

import spock.lang.Specification

import static java.time.LocalDate.parse
import static pl.allegro.training.kotlin.board.task.Column.DONE
import static pl.allegro.training.kotlin.board.task.Column.IN_PROGRESS
import static pl.allegro.training.kotlin.board.task.Column.TO_DO
import static pl.allegro.training.kotlin.board.task.Column.TO_VERIFY
import static pl.allegro.training.kotlin.board.task.SampleEvents.events

class TaskBoardServiceSpec extends Specification {

    TaskBoardService taskBoardService = new TaskBoardService(3)

    def "should return task board as of 2017-01-15"() {
        given: "there are list of task events"
            List<TaskEvent> events = events()
        and: "task events are added to task board"
            events.collect { taskBoardService.addEvent(it) }

        when: "task board as of 2017-01-15 is requested"
            TaskBoardDto result = taskBoardService.taskBoard(parse("2017-01-15"))
        then: "there are no TO_DO tasks"
            result.tasks[TO_DO] == []
        and: "there is one IN_PROGRESS task"
            result.tasks[IN_PROGRESS] == [new Task("Create video search", 3, IN_PROGRESS) ]
        and: "there are two TO_VERIFY tasks"
            result.tasks[TO_VERIFY] == [ new Task("Add playlist management", 3, TO_VERIFY),
                                   new Task("Implement video comments", 4, TO_VERIFY) ]
        and: "there are no DONE tasks"
            result.tasks[DONE] == []
    }
}
